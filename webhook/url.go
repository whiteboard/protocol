package webhook

const (
	// UrlAuth 用户认证
	UrlAuth = `/whiteboards/:appid/webhooks/auths/users/:userid`
	// UrlBoardAcl 面板授权
	UrlBoardAcl = `/whiteboards/:appid/webhooks/boards/:id/acl`
	// UrlUserAcl 用户授权
	UrlUserAcl = `/whiteboards/:appid/webhooks/users/:id/acl`
	// UrlGroupAcl 组授权
	UrlGroupAcl = `/whiteboards/:appid/webhooks/groups/:id/acl`
	// UrlBoardUserinfo 面板用户信息
	UrlBoardUserinfo = `/whiteboards/:appid/webhooks/boards/:boardId/users/:id/infos`
	// UrlGroupUserinfo 组用户信息
	UrlGroupUserinfo = `/whiteboards/:appid/webhooks/groups/:groupId/users/:id/infos`
	// UrlUserStatus 用户状态改变
	UrlUserStatus = `/whiteboards/:appid/webhooks/users/:id/statuses`
	// UrlBoardStatus 面板状态改变
	UrlBoardStatus = `/whiteboards/:appid/webhooks/boards/:id/statuses`
)
