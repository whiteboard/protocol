# Whiteboard（白板）

白板

# 关于用户

`userid`只是一个抽象的概念，接入方可以根据自己的需求纵一个唯一可以标识用户的字符串即可，可以是

- 用户名
- 用户编号

# 服务器接入

需要实现如下接口

- 回调
- gRPC

## 回调

### 认证

用来确认该账号是否可以登录白板服务器

#### 地址

whiteboards/{appid}/webhooks/auths/users/{userid}

#### 请求

已经封装好了请求数据，使用

- Golang：`github.com/whiteboard/protocol/webhook/AuthReq`

#### 响应

已经封装好了请求数据，使用

- Golang：`github.com/whiteboard/protocol/webhook/AuthRsp`

### 授权

用于确认该客户端能否进入某个面板

#### 地址

whiteboards/{appid}/webhooks/boards/{boardId}/acl

#### 请求

已经封装好了请求数据，使用

- Golang：`github.com/whiteboard/protocol/webhook/AclBoardReq`

#### 响应

已经封装好了请求数据，使用

- Golang：`github.com/whiteboard/protocol/webhook/AclBoardRsp`

### 授权

用来确认该用户是否可以收到白板服务器对他的通知

#### 地址

whiteboards/{appid}/webhooks/users/{userid}/acl

#### 方法

POST

#### 请求

已经封装好了请求数据，使用

- Golang：`github.com/whiteboard/protocol/webhook/AclUserRsp`

#### 响应

已经封装好了请求数据，使用

- Golang：`github.com/whiteboard/protocol/webhook/AclUserRsp`

### 授权

用来确认该用户是否可以加入某个组以及能否在组内发言

#### 地址

whiteboards/{appid}/webhooks/groups/{groupId}/acl

#### 方法

POST

#### 请求

已经封装好了请求数据，使用

- Golang：`github.com/whiteboard/protocol/webhook/AclGroupRsp`

#### 响应

已经封装好了请求数据，使用

- Golang：`github.com/whiteboard/protocol/webhook/AclGroupRsp`

### 用户信息

每次用户登录时，白板服务器会向应用请求该用户的详细信息，用于以下途径

- 白板客户端中的用户列表显示
- 确定用户的角色

#### 地址

whiteboards/{appid}/webhooks/boards/{boardId}/users/{userid}/infos

#### 方法

POST

#### 请求

已经封装好了请求数据，使用

- Golang：`github.com/whiteboard/protocol/webhook/UserinfoReq`

#### 响应

已经封装好了请求数据，使用

- Golang：`github.com/whiteboard/protocol/webhook/UserinfoRsp`

### 用户信息

当一个用户在组里面的信息，会用来做如下用途

- 组内上线通知
- 组内下线通知
- 组内角色

#### 地址

whiteboards/{appid}/webhooks/groups/{groupId}/users/{userid}/infos

#### 方法

POST

#### 请求

已经封装好了请求数据，使用

- Golang：`github.com/whiteboard/protocol/webhook/UserinfoReq`

#### 响应

已经封装好了请求数据，使用

- Golang：`github.com/whiteboard/protocol/webhook/UserinfoRsp`

### 用户状态通知

用户状态改变时会通知到应用方，用户状态通知包括

- 上线
- 下线

#### 地址

whiteboards/{appid}/webhooks/users/{userid}/statuses

#### 方法

PUT

#### 请求

已经封装好了请求数据，使用

- Golang：`github.com/whiteboard/protocol/webhook/UserStatusReq`

#### 响应

已经封装好了请求数据，使用

- Golang：`github.com/whiteboard/protocol/webhook/UserStatusRsp`
-

### 面板状态通知

面板会在状态发生改变时通知到应用方，状态有

- 已创建
- 准备中
- 已开始
- 已离开
- 已延迟
- 已终止

#### 地址

whiteboards/{appid}/webhooks/boards/{boardId}/statuses

#### 方法

PUT

#### 请求

已经封装好了请求数据，使用

- Golang：`github.com/whiteboard/protocol/webhook/BoardStatusReq`

#### 响应

已经封装好了请求数据，使用

- Golang：`github.com/whiteboard/protocol/webhook/BoardStatusRsp`

## gRPC

gRPC方便服务器调用

- board

## 客户端接入

客户端支持

- PC
- 移动端
- 网页端
- 小程序
